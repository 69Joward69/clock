//
//  ViewController.swift
//  Clock
//
//  Created by itadmin on 28/4/16.
//  Copyright © 2016 Phone. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    
    var timer: NSTimer?
    
    let clock = Clock()
    
    func updateTimeLabel()
    {
        let formatter = NSDateFormatter()
        formatter.timeStyle = .MediumStyle
        timeLabel.text = formatter.stringFromDate(clock.currentTime)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ViewController.updateTimeLabel), name: UIApplicationWillEnterForegroundNotification, object: nil)
        
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(ViewController.updateTimeLabel), userInfo: nil, repeats: true)
        
    }

    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        print(" viewWillAppear")
        updateTimeLabel()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
        return UIInterfaceOrientationMask.All
    }
    
    override func prefersStatusBarHidden() -> Bool
    {
        return true
    }
    
    deinit
    {
        if let timer = self.timer
        {
            timer.invalidate()
        }
    }

}

